<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-alias
// Langue: en
// Date: 11-03-2012 15:32:42
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'alias_description' => 'It is quite common to want to display a same content in multiple locations in a tree. While this practice is not desirable, and if it must always lead us to question the relevance of our sectioning, it is nonetheless necessary in some cases. This plugin makes it possible to quickly create aliases, to display the same content in multiple locations in a tree.',
	'alias_nom' => 'Articles Alias',
	'alias_slogan' => 'Allow to create aliases for articles',
);
?>
<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-alias
// Langue: ru
// Date: 11-03-2012 15:32:42
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'alias_description' => 'Позволяет создавать ссылки или ярлыки на статьи',
	'alias_nom' => 'Ссылки на статьи',
	'alias_slogan' => 'Позволяет создавать ссылки или ярлыки на статьи',
);
?>
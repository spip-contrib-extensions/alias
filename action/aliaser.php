<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function alias_aliaser_objet($id_article) {
	include_spip('base/abstract_sql');
	$row = sql_fetsel('id_rubrique', 'spip_articles', 'id_article=' . intval($id_article));
	$c = [
		'surtitre' => "<article$id_article|surtitre>",
		'titre' => "<article$id_article|titre>",
		'soustitre' => "<article$id_article|soustitre>",
		'descriptif' => "<article$id_article|descriptif>",
		'chapo' => "<article$id_article|chapo>",
		'texte' => "<article$id_article|texte>",
		'ps' => "<article$id_article|ps>",
	];
	$new = 0;
	if ($row) {
		include_spip('action/editer_article');
		$new = article_inserer($row['id_rubrique']);
		include_spip('inc/modifier');
		article_modifier($new, $c);
		// et on peut meme aliaser le portfolio
		$rows = sql_allfetsel('id_document', 'spip_documents_liens', "objet='article' AND id_objet=" . intval($id_article));
		foreach ($rows as $k => $row) {
			$rows[$k] = ['id_objet' => $new, 'id_document' => $row['id_document'], 'objet' => 'article'];
		}
		if (count($rows)) {
			sql_insertq_multi('spip_documents_liens', $rows);
		}
	}

	return $new;
}

function action_aliaser_dist() {

	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();
	$arg = explode('-', $arg);
	$type = 'article';
	if (preg_match(',^\w*$,', $arg[0])) {
		$type = $arg[0];
	}

	$id_article = alias_aliaser_objet(intval($arg[1]));
	$retour = parametre_url(urldecode(_request('redirect')), 'id_article', $id_article, '&');
	include_spip('inc/headers');
	redirige_par_entete($retour);
}

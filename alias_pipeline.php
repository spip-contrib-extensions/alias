<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function alias_affiche_droite($flux) {
	if ($flux['args']['exec'] == 'articles' || ($flux['args']['exec'] == 'article' && isset($flux['args']['id_article']))) {
		global $spip_lang_right;
		$out = '';
		$id_article = $flux['args']['id_article'] ?? 0;
		$out .= "<div class='box infos'>";

		$securiser_action = charger_fonction('securiser_action', 'inc');
		$url = $securiser_action('aliaser', "article-$id_article", str_replace('&amp;', '&', self(true)));
		$icone_horizontale = chercher_filtre('icone_horizontale');
		$out .= $icone_horizontale($url, _T('alias:create_alias'), 'alias-xx.svg');
		$out .= '</div>';
		$flux['data'] .= $out;
	}

	return $flux;
}
